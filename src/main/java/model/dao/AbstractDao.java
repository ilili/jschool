package model.dao;


import lombok.NonNull;
import org.hibernate.Criteria;
import org.hibernate.QueryParameterException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import persistence.BaseObject;
import util.HibernateHelper;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractDao<T> implements DaoCrud<BaseObject> {

	// TODO: 27.10.2019 exceptions
	@Override public Optional<BaseObject> get(@NonNull Number id) {
		BaseObject baseObject;
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			baseObject = session.get(BaseObject.class, id);

			session.getTransaction().commit();
		}
		return Optional.of(baseObject);
	}

	// TODO: 27.10.2019 pagination selection


	public List<BaseObject> getAll(Class<BaseObject> clazz) {
		List<BaseObject> objects;

		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			String tableName = getTableNameByClass(clazz);
			objects = session.createQuery("SELECT * FROM " + tableName, clazz).getResultList();
			session.getTransaction().commit();
		}
		return objects;
	}

	public Long getTotalAmount(Class clazz) {
		try (Session session = HibernateHelper.getSession()) {
			Long rowCount = 0L;
			if (BaseObject.class.isAssignableFrom(clazz)) {

				session.beginTransaction();

				Criteria criteria = session.createCriteria(clazz);
				criteria.setProjection(Projections.rowCount());

				List objects = criteria.list();

				if (objects != null) {
					rowCount = (Long) objects.get(0);
				}

				session.getTransaction().commit();
			}

			return rowCount;
		}
	}

	protected String getTableNameByClass(@NonNull Class<BaseObject> clazz) {
//		if (clazz.isAssignableFrom(BaseObject.class)) {}
		String result;
		switch ("persistence." + clazz.getName()) {
			case "User":
				result = "Users";
				break;
			case "Contract":
				result = "Contracts";
				break;
			case "Tariff":
				result = "Tariffs";
				break;
			case "TariffOption":
				result = "Tariff_options";
				break;
			case "TariffOptionsRestrictions":
				result = "Tariff_options_restrictions";
				break;
			default:
				throw new QueryParameterException("Incorrect class given (" + clazz.getCanonicalName() +
					                                  "), can't translate it to the table name.");
		}
		return result;
	}


	private Class getClassByName(@NonNull String clazzName) throws ClassNotFoundException {
		return Class.forName("persistence." + clazzName);
	}

	public void save(BaseObject baseObject) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			session.save(baseObject);

			session.getTransaction().commit();
		}
	}

	@Override public void update(BaseObject baseObject, Map<String, Object> params) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			if (params != null) {
				params.entrySet()
				      .forEach(entry -> mapSingleValue(baseObject, entry));
			}
			session.update(baseObject);

			session.getTransaction().commit();
		}
	}


	@Override public void delete(BaseObject baseObject) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			session.delete(baseObject);

			session.getTransaction().commit();
		}
	}


	private void mapSingleValue(Object object, Map.Entry<String, Object> mapEntry) {
		Field[] objFieldsArray = object.getClass().getDeclaredFields();
		List<Field> fieldsList = Arrays.asList(objFieldsArray);

		if (mapEntry != null) {
			fieldsList.stream()
			          .filter(field -> mapEntry.getKey().equals(field.getName())) // Class field's name equals to map key
			          .filter(field -> {                                          // Class field's type equals to map value
				          field.setAccessible(true);
				          boolean result = false;
				          Class fieldClass = field.getType();
				          result = fieldClass.equals(mapEntry.getValue().getClass());
				          return result;
			          })
			          .forEach(field -> {                                         // Set field value
				          try {
					          Field fieldToSet = object.getClass().getDeclaredField(field.getName());
					          fieldToSet.setAccessible(true);
					          fieldToSet.set(object, mapEntry.getValue());
				          }
				          catch (IllegalAccessException | NoSuchFieldException e) { e.printStackTrace(); }
			          });
		}
	}
}