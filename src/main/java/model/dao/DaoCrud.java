package model.dao;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DaoCrud<T> {

	Optional<T> get(Number id);

	List<T> getAll(Class<T> clazz);

	void save(T t);

	void update(T t, Map<String, Object> params);

	void delete(T t);
}