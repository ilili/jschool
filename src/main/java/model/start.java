package model;

import model.dao.DaoImplementation;
import persistence.User;
import persistence.UserRole;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class start {

	public static void main(String[] args) {

		DaoImplementation x = new DaoImplementation();


		User u1 = new User();
		u1.setLogin("user1");
		u1.setEmail("user1@example.com");
		u1.setPasswordHash("0000");
		u1.setUserRole(UserRole.ADMIN);
		u1.setFirstName("UserName1");
		u1.setLastName("LastName1");
		u1.setAddress("Address");
		u1.setPassport("psp1");
		u1.setActive(true);
		u1.setCreated(null);
		u1.setBirthDate(Timestamp.valueOf(LocalDateTime.now()));

		User u2 = new User();
		u2.setLogin("UserName2");
		u2.setEmail("user2@example.com");
		u2.setPasswordHash("qqqq");
		u2.setUserRole(UserRole.CLIENT);
		u2.setFirstName("UserName2");
		u2.setLastName("LastName2");
		u2.setAddress("Addr2");
		u2.setActive(false);
		u2.setPassport("psp2");
		u2.setBirthDate(Timestamp.valueOf(LocalDateTime.now()));

		x.save(u1);
		x.save(u2);

		x.delete(u2);

		Map<String, Object> updateU1 = new HashMap<>();
		updateU1.put("address", "UNKNOWN");
		updateU1.put("passwordHash", "----");

		x.update(u1, updateU1);
		x.delete(u1);

		System.out.println(x.getTotalAmount(User.class));
	}
}
