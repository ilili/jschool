package persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@Getter @Setter
@MappedSuperclass
public abstract class BaseObject {

	@Basic
	@Column(name = "IS_ACTIVE", columnDefinition = "BOOLEAN DEFAULT FALSE")
	boolean isActive;

	@Basic @Column(name = "CREATED_BY")
	protected int createdBy;

	@Basic @Column(name = "CREATED")
	protected Timestamp created;

	@Basic @Column(name = "CHANGED_BY")
	protected int changedBy;

	@Basic @Column(name = "LAST_CHANGE")
	protected Timestamp lastChange;
}
