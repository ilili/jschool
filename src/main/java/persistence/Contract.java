package persistence;

import javax.persistence.*;

@Entity
@Table(name = "Contracts")
public class Contract extends TableLongId {

	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "ID")
	private User user;

	// TODO: 18.10.2019 autogenerate with policy @SequenceGenerator
	@Basic
	@Column(name = "CONTRACT_NUMBER", length = 40, nullable = false, unique = true)
	private String contractNumber;

	@Basic
	@Column(name = "PHONE_NUMBER", length = 10, unique = true)
	private String phoneNumber;

	@ManyToOne
	@JoinColumn(name = "TARIFF_ID", referencedColumnName = "ID", nullable = false)
	private Tariff tariff;

	@Basic @Column(name = "TARIFF_OPTIONS")
	private String tariffOptions;
}