package persistence;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.util.List;

@Entity
@Table(name = "Tariffs")
public class Tariff extends TableIntId {

	@Basic
	@Column(name = "NAME", unique = true, length = 64, nullable = false)
	private String name;

	// todo control the float part
	@Digits(integer = 10, fraction = 2)
	@Column(name = "PRICE", nullable = false, columnDefinition = "DOUBLE(10,2) DEFAULT 0.00")
	private Double price;

	@OneToMany(mappedBy = "tariff")
	private List<Contract> contracts;

	@Column(name = "AVAILABLE_OPTIONS")
	private String availableOptions;

	@OneToMany(mappedBy = "tariff")
	private List<TariffOption> tariffOptions;
}
