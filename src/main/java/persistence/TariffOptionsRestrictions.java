package persistence;

import javax.persistence.*;

@Entity
@Table(name = "Tariff_options_restrictions")
public class TariffOptionsRestrictions extends TableIntId {

	@Basic
	@Column(name = "OPTIONS_MATCHER", nullable = false)
	private String optionsMatcher;

	@Column(name = "RELATIONSHIP")
	private int relationship;

	@Basic @Column(name = "OPTIONS_MANAGEABLE", nullable = false)
	private String optionsManageable;
}
