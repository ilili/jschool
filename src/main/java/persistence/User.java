package persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@EqualsAndHashCode(callSuper = true) @Data
@Entity
@Table(name = "Users")
public class User extends TableIntId {

	@Basic
	@Column(name = "LOGIN", unique = true, length = 64, nullable = false)
	private String login;

	@Basic
	@Column(name = "EMAIL", unique = true, length = 80, nullable = false)
	private String email;

	@Basic
	@Column(name = "PASSWORD_HASH", length = 32, nullable = false)
	private String passwordHash;

	// todo переделать на int с именами в отдельной таблице
//	@Basic
	@Column(name = "USER_ROLE", length = 20, nullable = false)
	@Enumerated(EnumType.STRING)
	private UserRole userRole;

	@Basic
	@Column(name = "FIRST_NAME", length = 128, nullable = false)
	private String firstName;

	@Basic
	@Column(name = "LAST_NAME", length = 128, nullable = false)
	private String lastName;

	@Basic
	@Column(name = "ADDRESS", length = 160, nullable = false)
	private String address;

	@Basic
	@Column(name = "PASSPORT", nullable = false)
	private String passport;

	@Column(name = "BIRTH_DATE", nullable = false)
	private Timestamp birthDate;

	@OneToMany(mappedBy = "user")
	private Collection<Contract> contracts;
}
