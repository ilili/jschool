package util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ResponseParser {

	public Map<String, String> extractBodyParamsFromPostRequest(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		BufferedReader bufferedReader = null;
		String content = "";

		try {
			bufferedReader = request.getReader(); //new BufferedReader(new InputStreamReader(inputStream));
			char[] charBuffer = new char[128];
			int bytesRead;
			while ((bytesRead = bufferedReader.read(charBuffer)) != -1) {
				sb.append(charBuffer, 0, bytesRead);
			}
		}
		catch (IOException ex) {
			// todo log it
		}
		finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				}
				catch (IOException ex) {
					// todo log it
				}
			}
		}
		content = sb.toString();
		return toMap(content);
	}

	private Map<String, String> toMap(String str) {
		Map<String, String> result = new HashMap<>();
		if (StringUtils.isNotEmpty(str)) {
			String[] parameters = str.split("&");
			List<String> parametersList = new ArrayList<>(Arrays.asList(parameters));
			result = parametersList.stream()
			                       .filter(el -> {
				                       String[] pair = el.split("=");
				                       return pair.length == 2 &&
					                       StringUtils.isNotEmpty(pair[0]) &&
					                       StringUtils.isNotEmpty(pair[1]);
			                       })
			                       .map(el -> el.split("="))
			                       .collect(Collectors.toMap(arr -> arr[0], arr -> arr[1]));
		}
		return result;
	}
}