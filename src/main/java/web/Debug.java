package web;

import util.Numerals;
import util.ResponseParser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.http.HttpConnectTimeoutException;
import java.util.Map;

@WebServlet(
	description = "Debug page"
//	,
//	urlPatterns = { "/" }
)
public class Debug extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		System.out.println("--== Debug get ==--");

		System.out.println(req);

		resp.setContentType("text/html;charset=utf-8");

		RequestDispatcher view = req.getRequestDispatcher("index.jsp");
		view.forward(req, resp);
	}

	@Override protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("--== Debug post ==--");

		ResponseParser rp = new ResponseParser();
		Map<String, String> results = rp.extractBodyParamsFromPostRequest(request);

		System.out.println(results); // TODO: 31.10.2019 remove

		if (true) { // fixme сейчас выбор номера ошибки не работает
//		if (!results.isEmpty()) { // fixme сейчас выбор номера ошибки не работает
			String errNumber = results.get("error-number");
//			if (Numerals.checkIsStringInt(errNumber)) {
			if (true) {
				Integer errorNumber = Numerals.stringToInteger(errNumber);
				System.out.println(results); // TODO: 31.10.2019 remove
					throw new HttpConnectTimeoutException(results.get("error-description")); // TODO: 31.10.2019 сделать выбор эксепшенов
			}
		}
//		super.doPost(req, resp);

		RequestDispatcher view = request.getRequestDispatcher("index.jsp");
		view.forward(request, resp);
	}
}
