package web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
	description = "List of all users")
public class UsersList extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {
		response.setContentType("text/html;charset=utf-8");
		request.setAttribute("user", "Smith");
		request.setAttribute("ttt", "zzz");
		RequestDispatcher view = request.getRequestDispatcher("users_list.jsp");
		view.forward(request, response);
	}
}
