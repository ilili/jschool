CREATE DATABASE IF NOT EXISTS `JSchool`;
USE JSchool;

CREATE TABLE `Users` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`LOGIN` VARCHAR(64) NOT NULL UNIQUE,
	`EMAIL` VARCHAR(80) NOT NULL UNIQUE,
	`PASSWORD_HASH` VARCHAR(32) NOT NULL,
	`USER_ROLE` ENUM('ADMIN', 'CLIENT') NOT NULL,
	`IS_ACTIVE` BOOLEAN DEFAULT FALSE,
	`FIRST_NAME` VARCHAR(128) NOT NULL DEFAULT '',
	`LAST_NAME` VARCHAR(128) NOT NULL DEFAULT '',
	`ADDRESS` VARCHAR(160) NULL DEFAULT '',
	`PASSPORT` VARCHAR(255) NULL DEFAULT '',
	`BIRTH_DATE` DATETIME(6) NOT NULL,
	`CREATED_BY` INT NULL,
	`CREATED` DATETIME(6) NULL,
	`CHANGED_BY` INT NULL,
	`LAST_CHANGE` DATETIME(6) NULL,
	PRIMARY KEY (`ID`), UNIQUE INDEX `Users_ID_uindex` (`ID`)
) COMMENT='Basic user data' COLLATE='utf8_bin' ENGINE=InnoDB
;


CREATE TABLE `Tariffs` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`NAME` VARCHAR(64) NOT NULL,
	`PRICE` DOUBLE(10,2) NOT NULL DEFAULT 0.00,
	`IS_ACTIVE` BOOLEAN DEFAULT FALSE,
	`AVAILABLE_OPTIONS` JSON NULL,
	`CREATED_BY` INT NULL,
	`CREATED` DATETIME(6) NULL,
	`CHANGED_BY` INT NULL,
	`LAST_CHANGE` DATETIME(6) NULL,
	PRIMARY KEY (`ID`),
	UNIQUE INDEX `NAME` (`NAME`)
)
COMMENT='Available tariffs'
COLLATE='utf8_bin'
ENGINE=InnoDB
;


CREATE TABLE `Contracts` (
	`ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`USER_ID` INT NULL DEFAULT NULL,
	`CONTRACT_NUMBER` VARCHAR(40) NOT NULL UNIQUE,
	`PHONE_NUMBER` VARCHAR(10) UNIQUE,
	`TARIFF_ID` INT NOT NULL,
	`TARIFF_OPTIONS` JSON NULL,
	`IS_ACTIVE` BOOLEAN DEFAULT FALSE,
	`CREATED_BY` INT NULL,
	`CREATED` DATETIME(6) NULL,
	`CHANGED_BY` INT NULL,
	`LAST_CHANGE` DATETIME(6) NULL,
	PRIMARY KEY (`ID`),
	UNIQUE INDEX `ID` (`ID`),
	INDEX `User_ID` (`USER_ID`),
	CONSTRAINT `Contract's owner` FOREIGN KEY (`USER_ID`) REFERENCES `Users` (`ID`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `Selected tariff` FOREIGN KEY (`TARIFF_ID`) REFERENCES `Tariffs` (`ID`) ON UPDATE CASCADE ON DELETE CASCADE
)
COMMENT='Contracts of particular user'
COLLATE='utf8_bin'
ENGINE=InnoDB
;


CREATE TABLE `Tariff_options` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`TARIFF_ID` INT NOT NULL,
	`NAME` VARCHAR(120) NOT NULL,
	`MATCHER` VARCHAR(128) NOT NULL UNIQUE,
	`PRICE` DOUBLE(10,2) NOT NULL DEFAULT 0.00,
	`ACTIVATION_PRICE` DECIMAL(10,2) NOT NULL DEFAULT 0.00,
	`IS_ACTIVE` BOOLEAN DEFAULT FALSE,
	`CREATED_BY` INT NULL,
	`CREATED` DATETIME(6) NULL,
	`CHANGED_BY` INT NULL,
	`LAST_CHANGE` DATETIME(6) NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `Parent tariff` FOREIGN KEY (`TARIFF_ID`) REFERENCES `Tariffs` (`ID`) ON UPDATE CASCADE ON DELETE CASCADE
)
COMMENT='Tariff options'
COLLATE='utf8_bin'
ENGINE=InnoDB
;


CREATE TABLE `Tariff_options_restrictions` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`TARIFF_ID` INT NOT NULL,
	`OPTIONS_MATCHER` JSON NOT NULL,
	`RELATIONSHIP` INT NOT NULL,
	`OPTIONS_MANAGEABLE` JSON NOT NULL,
	`CREATED_BY` INT NOT NULL,
	`CREATED` DATETIME(6) NULL,
	`CHANGED_BY` INT NULL,
	`LAST_CHANGE` DATETIME(6) NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `Restrictions for tariff` FOREIGN KEY (`TARIFF_ID`) REFERENCES `Tariffs` (`ID`) ON UPDATE CASCADE ON DELETE CASCADE
) COMMENT='Table of tariff options that are compatible or not compatible to each other' COLLATE='utf8_bin'
;