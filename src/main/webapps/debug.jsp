<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<%@include file="common/head.jsp" %>
<body>

    <div class="available-field">
        <div class="content-container">
            <div class="page-header">Сломать что-нибудь</div>
                <form method="post" action="debug" novalidate="" id="debug-form">

                        <input class="input" type="text" name="error-number" autocapitalize="none" autocorrect="off" required="" minlength="3" maxlength="60" placeholder="Код ошибки" tabindex="0">
                        <input class="input" type="text" name="error-description" autocapitalize="none" autocorrect="off" required="" minlength="3" maxlength="60" placeholder="Описание ошибки" tabindex="0">
                        <button type="submit" title="Сломать">Сломать</button>

                </form>
            </div>
        </div>
</body>
</html>